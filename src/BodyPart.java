import java.awt.Color;
import java.awt.Graphics;

public class BodyPart {
	
	private int xCoor, yCoor, width, height, ts;
	
	public BodyPart(int xCoor, int yCoor, int tileSize, int ts) {
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		this.ts = ts;
		width = tileSize;
		height = tileSize;
	}
	
	public void tick() {
		
	}
	
	public void draw(Graphics g) {
		if(ts == 0){//COMUM
			g.setColor(Color.GREEN);
			g.fillRect(xCoor * width, yCoor * height, width, height);
		}
		
		if(ts == 1) {//KITTY
			g.setColor(Color.PINK);
			g.fillRect(xCoor * width, yCoor * height, width, height);
		}
		
		if(ts == 2) {//STAR
			g.setColor(Color.YELLOW);
			g.fillRect(xCoor * width, yCoor * height, width, height);
		}
	}

	public int getxCoor() {
		return xCoor;
	}

	public void setxCoor(int xCoor) {
		this.xCoor = xCoor;
	}

	public int getyCoor() {
		return yCoor;
	}

	public void setyCoor(int yCoor) {
		this.yCoor = yCoor;
	}
}
