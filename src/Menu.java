import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Menu extends JPanel implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int tc;

	public int getTc() {
		return tc;
	}

	public void setTc(int tc) {
		this.tc = tc;
	}
	
	public Menu() {
		menu();
	}
	
	public void menu() {
		
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(500, 500));
		
		setLayout(null);
		
		
		JLabel titulo = new JLabel("Snake Game");
		titulo.setFont(new Font("Lucida Handwriting", Font.PLAIN, 30));
		titulo.setBounds(150, -18, 225, 76);
		add(titulo);
		
		JLabel escolha = new JLabel("Escolha o tipo: ");
		escolha.setFont(new Font("Helvetica", Font.PLAIN, 15));
		escolha.setBounds(63, 69, 158, 25);
		add(escolha);
		
		JLabel comum = new JLabel("Comum");
		comum.setFont(new Font("Helvetica", Font.PLAIN, 15));
		comum.setBounds(63, 150, 86, 20);
		add(comum);
		
		JLabel kitty = new JLabel("Kitty");
		kitty.setFont(new Font("Helvetica", Font.PLAIN, 15));
		kitty.setBounds(63, 250, 86, 20);
		add(kitty);

		JLabel star = new JLabel("Star");
		star.setFont(new Font("Helvetica", Font.PLAIN, 15));
		star.setBounds(63, 350, 86, 20);
		add(star);
		
		JLabel ik = new JLabel("Atravessa barreiras");
		ik.setFont(new Font("Helvetica", Font.PLAIN, 15));
		ik.setBounds(30, 270, 150, 20);
		add(ik);
		
		JLabel is = new JLabel("Dobro de Pontos");
		is.setFont(new Font("Helvetica", Font.PLAIN, 15));
		is.setBounds(30, 370, 150, 20);
		add(is);
		
		ButtonGroup grupobutton = new ButtonGroup();
		
		JRadioButton kittyb = new JRadioButton("");
		kittyb.setBackground(Color.WHITE);
		kittyb.setBounds(180, 250, 25, 23);
		kittyb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setTc(1);
				kittyb.setSelected(true);	
			}
		});
		add(kittyb);
		
		JRadioButton comumb = new JRadioButton("");
		comumb.setBackground(Color.WHITE);
		comumb.setSelected(true);
		comumb.setBounds(180, 150, 25, 23);
		comumb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setTc(0);
				comumb.setSelected(true);	
			}
		});
		add(comumb);
		
		JRadioButton starb = new JRadioButton("");
		starb.setBackground(Color.WHITE);
		starb.setBounds(180, 350, 25, 23);
		starb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setTc(2);
				starb.setSelected(true);	
			}	
		});
		add(starb);
		
		grupobutton.add(kittyb);
		grupobutton.add(comumb);
		grupobutton.add(starb);

		JButton start = new JButton();
		start.setText("START");
		start.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				JFrame game = new JFrame();
				
				Gamepanel painel =  new Gamepanel(tc);
				
				game.add(painel);	
				game.setResizable(false);
				game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				game.setTitle("Snake Game");
				game.setVisible(true);
				game.pack();
				game.setLocationRelativeTo(null);		
			}
		});		
		start.setFont(new Font("Helvetica", Font.ITALIC, 15));
		start.setBounds(250, 150, 197, 44);
		add(start);


	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
