import java.awt.Color;
import java.awt.Graphics;

public class Fruits {
	
	private int xCoor, yCoor, width, height, type;
	
	public Fruits(int xCoor, int yCoor, int tileSize, int type) {
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		this.type = type;
		width = tileSize;
		height = tileSize;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void tick() {
		
	}
	
	public void draw(Graphics g) {
		if(type == 0) {//Simple
			g.setColor(Color.RED);
			g.fillRect(xCoor * width, yCoor * height, width, height);
		}
		
		else if(type == 1) {//BOMB
			g.setColor(Color.DARK_GRAY);
			g.fillRect(xCoor * width, yCoor * height, width, height);
		}
		
		if(type == 2) {//BIG
			g.setColor(Color.BLUE);
			g.fillRect(xCoor * width, yCoor * height, width, height);
		}
		
		if(type == 3) {//DECREASE
			g.setColor(Color.ORANGE);
			g.fillRect(xCoor * width, yCoor * height, width, height);
		}
	
	}

	public int getxCoor() {
		return xCoor;
	}

	public void setxCoor(int xCoor) {
		this.xCoor = xCoor;
	}

	public int getyCoor() {
		return yCoor;
	}

	public void setyCoor(int yCoor) {
		this.yCoor = yCoor;
	}

}
