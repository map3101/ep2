import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Gamepanel extends JPanel  implements Runnable, KeyListener, ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	public static final int WIDTH = 500, HEIGHT = 500;
	
	private Thread thread;
	
	private boolean running;
	
	private boolean right = true, left = false, up = false, down = false;
	
	private BodyPart b;
	private ArrayList<BodyPart> snake;
	
	private Fruits fruta;
	private ArrayList<Fruits> f;
	private int ref =0;
	
	private Random r;
	private Random tfruit;
	

	private int xCoor = 10, yCoor = 10, size = 5;
	
	private int score = 0;
	
	private int snaketype;
	
		
	private long tempoi;
	
	public Gamepanel(int snaketype) {
		
		this.snaketype = snaketype;
		
		setFocusable(true);
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		addKeyListener(this);
		
		snake = new ArrayList<BodyPart>();
		f = new ArrayList<Fruits>();
		
		r = new Random();
		tfruit = new Random();
		
		start();
	}
	
	public void start(){
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void retryb() {
		
		int o = JOptionPane.showConfirmDialog(null, "Game Over\nPontuação: " + score + "\nRetry?");
		
		if (o == 0) {
			JFrame reset = new JFrame();
			Gamepanel novo_painel = new Gamepanel(snaketype);
			
			reset.add(novo_painel);
			reset.setTitle("Snake Game");
			
			reset.pack();
			reset.setVisible(true);
			reset.setLocationRelativeTo(null);
			reset.setLayout(null);
		}
		
		else if(o == 1) {
			new Main();
		}
		
		else {System.exit(0);}
	}
			
	public void stop() {
		
		retryb();
				
			running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void tick() {
		if(snake.size() == 0) {
			b = new BodyPart(xCoor, yCoor, 10, snaketype);
			snake.add(b);
		}
		    
			if(right) xCoor++;
			if(left) xCoor--;
			if(up) yCoor--;
			if(down) yCoor++;
			
			
			
			b = new BodyPart(xCoor, yCoor, 10, snaketype);
			snake.add(b);
			
			if(snake.size() > size) {
				snake.remove(0);
			}
		
		if(f.size() == 0) {
			int xCoor = r.nextInt(49);
			int yCoor = r.nextInt(49);
			int type = tfruit.nextInt(4);
			
			if(ref <= 1 | ref%3 != 0) {
				fruta = new Fruits(xCoor, yCoor, 10, 0);
				f.add(fruta);
				ref++;
			}
			
			else if(ref > 1 && ref%3 == 0){
				fruta = new Fruits(xCoor, yCoor, 10, type);
				f.add(fruta);
				ref++;
			}
			tempoi = System.nanoTime();
		}
		
		for(int i = 0; i < f.size(); i++) {
			if(xCoor == f.get(i).getxCoor() && yCoor == f.get(i).getyCoor()) {
				if(f.get(i).getType() == 0) {
					size++;
					f.remove(i);
					if(snaketype == 2)score+=2;
					else score++;
					i++;
				}
				
				else if(f.get(i).getType() == 1) {
					stop();
				}
				
				else if(f.get(i).getType() == 2) {
					size++;
					f.remove(i);
					if(snaketype == 2)score+= 4;
					else score+=2;
					i++;
				}
				
				else if(f.get(i).getType() == 3) {
					snake.clear();
					size = 5;
					b = new BodyPart(xCoor, yCoor, 10, snaketype);
					snake.add(b);
					f.remove(i);
					i++;	
				}
			}
			
			long tempoa = System.nanoTime();
			if(((tempoa - tempoi)/1000000000)>10) {
				f.remove(i);
				i++;
			}
		}
		
		for(int i = 0; i < snake.size(); i++) {
			if(xCoor == snake.get(i).getxCoor() && yCoor == snake.get(i).getyCoor()) {
				if(i != snake.size() - 1) {
					stop();
				}
			}
		}
		
		if(xCoor < 0 || xCoor > 49 || yCoor < 0 || yCoor > 49) {
			stop();
		}
		
		if(snaketype == 0 || snaketype == 2) {
			for(int i = 0; i < snake.size(); i++) {
				for(int j = 0; j < 13; j++) {
					if(snake.get(i).getxCoor() == 30 && snake.get(i).getyCoor() == j + 2 || snake.get(i).getxCoor() == 10 && snake.get(i).getyCoor() == j + 25
							|| snake.get(i).getxCoor() == 35 && snake.get(i).getyCoor() == j + 30) {
						stop();
					}
				}	
			}
 
		}
		
	}
	
	public void paint(Graphics g) {
		g.clearRect(0, 0, WIDTH, HEIGHT);
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		for(int i = 0; i < WIDTH/10; i++) {
			g.drawLine(i*10, 0, i*10, HEIGHT);
		}
		
		for(int i = 0; i < HEIGHT/10; i++) {
			g.drawLine(0, i*10, HEIGHT, i*10);
		}
		
		for(int i = 0; i < snake.size(); i++) {
			snake.get(i).draw(g);
		}
		
		for(int i = 0; i < f.size(); i++) {
			f.get(i).draw(g);
		}
	
		g.setColor(Color.MAGENTA);
		for(int i = 0; i < 10; i++) {
			g.fillRect(300, i+20, 10, 120);
			g.fillRect(100, i+250, 10, 120);
			g.fillRect(350, i+300, 10, 120);
		}
	
	}

	private long FPS;
	private long startTime;
	private long URDTimeMillis;
	private long waitTime;
	private long targetTime;
	
	@Override
	public void run() {
		FPS = 20;
		targetTime = 1000/ FPS;
		
		while(running) {
			startTime = System.nanoTime();
			
			tick();
			repaint();    
				
			URDTimeMillis = (System.nanoTime() - startTime) / 1000000;
			waitTime = targetTime - URDTimeMillis;
			
			try {
				Thread.sleep(waitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}


	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if(key == KeyEvent.VK_RIGHT && !left) {
			right = true;
			up = false;
			down = false;
		}
		
		if(key == KeyEvent.VK_LEFT && !right) {
			left = true;
			up = false;
			down = false;
		}
		
		if(key == KeyEvent.VK_UP && !down) {
			up = true;
			left = false;
			right = false;
		}
		
		if(key == KeyEvent.VK_DOWN && !up) {
			down = true;
			left = false;
			right = false;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}