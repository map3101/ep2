# EP2 - Snake Game
Aluno : Marcelo Aiache Postiglione 18/0126652
Professor: Renato

## Descrição

Para este EP o objetivo será implementar o famoso jogo Snake (também conhecido como "jogo da cobrinha") é um jogo que ficou conhecido por diversas versões cuja versão inicial começou com o jogo Blockade de 1976, sendo feitas várias imitações em vídeo-games e computadores. No fim dos anos 90 foi popularizado em celulares da Nokia que vinham com o jogo já incluso.
O jogador controla uma longa e fina criatura que se arrasta pela tela, coletando comida (ou algum outro item), não podendo colidir com seu próprio corpo ou as "paredes" que cercam a área de jogo. Cada vez que a serpente come um pedaço de comida, seu rabo cresce, aumentando a dificuldade do jogo. O usuário controla a direção da cabeça da serpente (para cima, para baixo, esquerda e direita) e seu corpo segue.

## Tipos de Snakes

* **Comum:** Verde, sem habilidades
* **Kitty:** Rosa, atravessa as barreiras
* **Star:** Amarela, Tem o dobro de pontos;

## Tipos de frutas

* **Simple Fruit:** (VERMELHA)Fruta comum de cor, dá um ponto e aumenta o tamanho da cobra.
* **Bomb Fruit:** (CINZA ESCURO)Essa fruta leva a morte da Snake.
* **Big Fruit:** (AZUL)Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
* **Decrease Fruit:** (LARANJA)Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos.

## Comandos

Direcionais do teclado